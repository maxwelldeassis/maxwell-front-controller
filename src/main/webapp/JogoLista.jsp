<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Jogos</title>
        <style>
            table tr{
                text-align: left!important;

            }
            table, table tr td, table tr th{
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>Jogos</h1>
        <table>
            <tr>
                <th>Id</th>                
                <th>Nome</th>    
                <th>Genero</th>    
                <th>Produtora</th>    
                <th>Plataforma</th>    
                <th>Faixa Etaria</th>
            </tr>
            <c:forEach var="jogo" items="${jogos}">
                <tr>
                    <td><a href="Controller?acao=SalvarFilme&id=${jogo.id}">${jogo.id}</a>                                        </td>
                    <td>${jogo.nome}</td>
                    <td>${jogo.genero}</td>
                    <td>${jogo.produtora}</td>
                    <td>${jogo.plataforma}</td>
                    <td>${jogo.faixaetaria}</td>
                    <td><a href="Controller?acao=ExcluirFilme&id=${jogo.id}">Deletar</a></td>
                </tr>

            </c:forEach>
        </table>
        <a href="index.html">Home</a>
    </body>
</html>
