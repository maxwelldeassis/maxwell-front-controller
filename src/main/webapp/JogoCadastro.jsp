<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Jogos</title>
    </head>
    <body>
        <h1>Cadastro de jogos</h1>
        <form action="Controller">
            <input type="hidden" name="acao" value="CadastrarJogo"/>
            <input type="hidden" name="id" value="<c:out value="${jogo.id}" />"/>
            <br><label>Nome</label><input type="text" name="nome" value="<c:out value="${jogo.nome}" />">
            <br><label>Genero</label><input type="text" name="genero" value="<c:out value="${jogo.genero}" />">
            <br><label>Produtora</label><input type="text" name="produtora" value="<c:out value="${jogo.produtora}" />">
            <br><label>Plataforma</label><input type="text" name="plataforma" value="<c:out value="${jogo.plataforma}" />">
            <br><label>Faixa Etaria</label><input type="text" name="faixaetaria" value="<c:out value="${jogo.faixaetaria}" />">
            <button type="submit">Cadastrar</button>
        </form>
    </body>
</html>
