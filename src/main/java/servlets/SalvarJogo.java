package servlets;

import bean.Jogo;
import dao.JogoDao;
import interfaces.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maxwell
 */
public class SalvarJogo implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        JogoDao dao = new JogoDao();

        String id = request.getParameter("id");

        Jogo jogo;

        if (id == null || "".equals(id)) {
            jogo = new Jogo();
        } else {
            jogo = dao.retornaJogo(Integer.parseInt(id));
        }

        request.setAttribute("jogo", jogo);

        return "/JogoCadastro.jsp";
    }

}
