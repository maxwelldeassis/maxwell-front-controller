package servlets;

import dao.JogoDao;
import interfaces.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maxwell
 */
public class CadastrarJogo implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        JogoDao dao = new JogoDao();

        String id = request.getParameter("id");

        if (id == null || id.isEmpty() || id.equalsIgnoreCase("0")) {

            dao.adicionarJogo(
                    request.getParameter("nome"),
                    request.getParameter("genero"),
                    request.getParameter("produtora"),
                    request.getParameter("plataforma"),
                    request.getParameter("faixaetaria")
            );

        } else {
            dao.atualizarJogo(
                    Integer.parseInt(id), 
                    request.getParameter("nome"),
                    request.getParameter("genero"),
                    request.getParameter("produtora"),
                    request.getParameter("plataforma"),
                    request.getParameter("faixaetaria"));
        }

        return "/Controller?acao=ListarJogo";
    }
}
