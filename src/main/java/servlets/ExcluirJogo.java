package servlets;

import dao.JogoDao;
import interfaces.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maxwell
 */
public class ExcluirJogo implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        JogoDao dao = new JogoDao();

        dao.removerJogo(Integer.parseInt(request.getParameter("id")));

        return "/Controller?acao=ListarJogo";
    }

}
