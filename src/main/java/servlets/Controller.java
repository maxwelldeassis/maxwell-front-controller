package servlets;

import interfaces.Acao;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maxwell
 */
@WebServlet(name = "Controller", urlPatterns = {"/Controller"})
public class Controller extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        String parametro = request.getParameter("acao");
        String nomeDaClasse = "servlets." + parametro;

        try {
            Class classe = Class.forName(nomeDaClasse);
            
            Acao acao = (Acao) classe.newInstance();
            
            String pagina = acao.executar(request, response);
            
            request.getRequestDispatcher(pagina).forward(request, response);
            
        } catch (Exception e) {
            throw new ServletException(
                    e.getMessage() + "Stack trace: "
                            + "A lógica de negócios causou uma exceção:" + Arrays.toString(e.getStackTrace()), e);
        }
    }
}
