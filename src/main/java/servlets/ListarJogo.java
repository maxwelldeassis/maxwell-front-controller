package servlets;

import dao.JogoDao;
import interfaces.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maxwell
 */
public class ListarJogo implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        JogoDao dao = new JogoDao();

        request.setAttribute("jogos", dao.retornaJogos());

        return "/JogoLista.jsp";
    }
}
