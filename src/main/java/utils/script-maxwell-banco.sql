/**
 * Author:  Maxwell
 * Created: 25/06/2017
 */

#============================================
DROP SCHEMA IF EXISTS MAXWELLBANCO;
CREATE SCHEMA MAXWELLBANCO;
USE MAXWELLBANCO;
#============================================
CREATE TABLE JOGO (
    ID INT AUTO_INCREMENT,
    NOME VARCHAR(80) NOT NULL,
    GENERO VARCHAR(80),
    PRODUTORA VARCHAR(80),
	PLATAFORMA VARCHAR(80),
    FAIXAETARIA VARCHAR(10),
    PRIMARY KEY (ID)
);

#============================================
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Battlefield 3", "Tiro", "EA Games", "Microsoft Windows, Xbox 360, PlayStation 3", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Brain Age", "Puzzle", "Nintendo", "Nintendo DS", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty 4: Modern Warfare", "Tiro", "Infinity Ward", "Windows, Xbox 360, Mac OS X, PlayStation 3, Wii", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty: Black Ops", "Tiro", "Infinity Ward", "Microsoft Windows, Xbox 360, PlayStation 3, Wii", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty: Black Ops II", "Tiro", "Infinity Ward", "Windows, Xbox 360, PlayStation 3, Wii U", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty: Ghosts", "Tiro", "Infinity Ward", "Windows, Xbox 360, Xbox One, PlayStation 3, PlayStation 4, Wii U", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty: Modern Warfare 2", "Tiro", "Infinity Ward", "Microsoft Windows, Xbox 360, PlayStation 3", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty: Modern Warfare 3", "Tiro", "Infinity Ward", "Microsoft Windows, Xbox 360, PlayStation 3, Wii", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Call of Duty: World at War", "Tiro", "Infinity Ward", "Windows, Xbox 360, PlayStation 3, Wii", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Diablo III", "RPG", "Blizzard", "Microsoft Windows, OS X, Xbox 360, PlayStation 3, Xbox One, PlayStation 4", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Grand Theft Auto III", "Ação", "Rockstar", "Windows, Xbox, PlayStation 2", "18+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Grand Theft Auto IV", "Ação", "Rockstar", "PlayStation 3, Xbox 360, Microsoft Windows", "18+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Grand Theft Auto V", "Ação", "Rockstar", "PlayStation 3, Xbox 360, PlayStation 4, Xbox One, Windows", "18+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Grand Theft Auto: San Andreas", "Ação", "Rockstar", "PlayStation 2, Microsoft Windows, Xbox, Mac OS X, Xbox 360", "18+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Grand Theft Auto: Vice City", "Ação", "Rockstar", "Windows, Xbox, PlayStation 2", "18+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Kinect Adventures!", "Aventura", "Microsoft Games", "Xbox 360", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Lemmings", "Puzzle", "Rockstar", "Amiga, outros", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Mario Kart DS", "Corrida", "Nintendo", "Nintendo DS", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Mario Kart Wii", "Corrida", "Nintendo", "Wii", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Need for Speed: Most Wanted", "Corrida", "EA Games", "PlayStation 2, Xbox, Xbox 360, GameCube, Windows, Game Boy Advance, Nintendo DS", "10+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("New Super Mario Bros.", "Plataforma", "Nintendo", "Nintendo DS", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("New Super Mario Bros. Wii", "Plataforma", "Nintendo", "Wii", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Nintendogs", "Simulação", "Nintendo", "Nintendo DS", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Overwatch", "Tiro", "Blizzard", "Windows, Xbox One, PlayStation 4", "13+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Pokémon Black e White", "RPG", "Nintendo", "Nintendo DS", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Pokémon Diamond e Pearl", "RPG", "Nintendo", "Nintendo DS", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Pokémon Gold eSilver", "RPG", "Nintendo", "Game Boy, Game Boy Color", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Pokémon Red, Blue e Green", "RPG", "Nintendo", "Game Boy", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Pokémon Ruby e Sapphire", "RPG", "Nintendo", "Game Boy Advance", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Sonic the Hedgehog", "Plataforma", "SEGA", "Sega Mega Drive/Genesis", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Super Mario Bros.", "Plataforma", "Nintendo", "Nintendo Entertainment System", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Super Mario Bros. 3", "Plataforma", "Nintendo", "Nintendo Entertainment System", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Super Mario Land", "Plataforma", "Nintendo", "Game Boy", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Super Mario World", "Plataforma", "Nintendo", "Super Nintendo Entertainment System", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Tetris", "Puzzle", "Tetris Holding", "Game Boy", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("The Elder Scrolls V: Skyrim", "RPG", "Bethesda", "Microsoft Windows, PlayStation 3, Xbox 360, PlayStation 4, Xbox One, Nintendo Switch", "18+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("The Sims", "Simulação", "Maxis", "Windows, Mac OS", "12+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("The Sims 2", "Simulação", "Maxis", "Windows, OS X", "16+");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Wii Fit", "Wii Party", "Nintendo", "Wii", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Wii Fit Plus", "Wii Party", "Nintendo", "Wii", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Wii Play", "Wii Party", "Nintendo", "Wii", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Wii Sports", "Wii Party", "Nintendo", "Wii", "Livre");
INSERT INTO JOGO (NOME, GENERO, PRODUTORA, PLATAFORMA, FAIXAETARIA) VALUES ("Wii Sports Resort", "Wii Party", "Nintendo", "Wii", "Livre")