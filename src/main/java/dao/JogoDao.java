package dao;

import bean.Jogo;
import sessionFactory.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Maxwell
 */
public class JogoDao {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarJogo(String nome, String genero, String produtora, String plataforma, String faixaEtaria) {
        Session session = fabrica.openSession();
        session.beginTransaction();

        Jogo jogo = criarJogo(nome, genero, produtora, plataforma, faixaEtaria);
        session.save(jogo);

        session.getTransaction().commit();
        session.close();
    }

    public void atualizarJogo(int id, String nome, String genero, String produtora, String plataforma, String faixaEtaria) {
        Session session = fabrica.openSession();
        session.beginTransaction();

        Jogo jogo = this.retornaJogo(id);
        jogo.setNome(nome);
        jogo.setProdutora(produtora);
        jogo.setPlataforma(plataforma);
        jogo.setFaixaetaria(Integer.parseInt(faixaEtaria));

        session.update(jogo);

        session.getTransaction().commit();
        session.close();
    }

    public void removerJogo(int id) {
        Jogo jogo = this.retornaJogo(id);

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.delete(jogo);

        session.getTransaction().commit();
        session.close();
    }

    public Jogo retornaJogo(int id) {
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Jogo where id = :jogo");
        Jogo jogo = (Jogo) q.setParameter("jogo", id).uniqueResult();

        session.close();
        return jogo;
    }

    public List<Jogo> retornaJogos() {
        List<Jogo> retorno;
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Jogo");
        retorno = q.list();
        session.close();
        return retorno;
    }

    public Jogo criarJogo(String nome,
            String genero,
            String produtora,
            String plataforma,
            String faixaEtaria) {
        Jogo jogo = new Jogo();
        jogo.setNome(nome);
        jogo.setProdutora(produtora);
        jogo.setPlataforma(plataforma);
        jogo.setFaixaetaria(Integer.parseInt(faixaEtaria));
        return jogo;
    }
}
