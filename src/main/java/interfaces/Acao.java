package interfaces;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public interface Acao {
    String executar(HttpServletRequest request,HttpServletResponse response) throws Exception;
}
